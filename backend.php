<?php

/* This code is INCOMPLETE!
 *
 * It should begin to read the file, but it will not output anything, at least yet; that is part of your assignment.
 *
 */


require_once('./classes.php');

$storage_file = "./storage.txt";

unset($file_handle);



$csvfile = fopen($storage_file,'r');

while(!feof($csvfile)) {

	    $allfields[] = fgetcsv($csvfile);
}

/*$rec_num = 1;
foreach ($allfields as $row){
	echo "<br><h5>Record Number $rec_num</h5>";
	echo "First Name: $row[0]<br>";
	echo "Last Name: $row[1]<br>";
	echo "Age: $row[2]<br>";
	echo "Income: $row[3]<br>";
	$rec_num++;
} */

/* as we saw at the end of class, the $allfields variable above is an array composed of arrays.  Specifically, each array within the bigger array is an array composed of each of the four elements in the csv file (storage.txt) in order.
 *
 * As such, you should now create an array of OBJECTS, of the class type "subject" (which is the class we have invented for this exercise) So, again, it should be an array of objects, composed of ALL of the objects in our "database" (which, in this case, is simply a flat-file)
 */

$subject_array = [];

foreach ($allfields as $row) {
	$currsub = new subject();

	$currsub->setFirstname($row[0]);
	$currsub->setLastname($row[1]);
	$currsub->setAge($row[2]);
	$currsub->setIncome($row[3]);

	$subject_array[] = $currsub;
}
//print_r($subject_array);
/* NEXT, after that, create a well formed "report" that returns the following information for EVERY object in the "database," some
thing like

 * Subject Name: Firstname Lastname
 * Age: age
 * Income: Income
 * Subject Code : color food
 *
 * Hopefully this is where you begin to see the possibe advantages of OOP; here we can very effectively reuse the code we've already created to provide the subject code (and note, the subject code is not actually stored in the database, and if the coding methodology changes, we're still good)
 */
$rec_num = 1;

foreach ($subject_array as $currsub) {
	echo "<br>";
	echo "<h2> Record number $rec_num </h2>";
	echo "<br><h3> Subject name: " . $currsub->getFirstname() . " " . $currsub->getLastname();
	echo "<br>Subject age: " . $currsub->getAge();
	echo "<br>Subject income: " . $currsub->getIncome();
	echo "<br>Subject Code Name: " . $currsub->agetocolor() . " " . $currsub->incometofood() . "</h3>";
	$rec_num++;
}
?>